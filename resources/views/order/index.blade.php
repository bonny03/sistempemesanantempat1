@extends('layouts.app')

@section('content')
<style type="text/css">
.card-header {
    background-color: aquamarine;
}
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">

                <div class="card-header"><i class="fas fa-database"> Order Data</div></i>
                <div class="card-body">
                <a href="{{ route('order.create') }}" class="btn btn-danger btn-sm"><i class="fas fa-plus-square"> Add Order</i></a>
                <hr>
                @include('notification')
                <table class="table table-bordered" id="users-table">
                    <thead>
                    <label>
                            Search:
                            <input type="search" class placeholder aria-controls="users-table">
                        <thead>
                            <tr>
                                <th><i class ="far fa-sticky-note"> No </th></i>
                                <th>Order Name</th>
                                <th>Order Price</th>
                                <th>Order Type</th>
                                <th>Order Amount</th>
                                <th>Jumlah</th>
                                <th>Edit</th>
                                <th>Delete</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; ?>

                          @foreach ($order as $item)
                          <tr>
                          <td>{{$no}}</td>
                          <td>{{$item->room_name}}</td>
                          <td>{{$item->room_price}}</td>
                          <td>{{$item->room_type}}</td>
                          <td>{{$item->room_amount}}</td>
                          <td>{{ $item->jumlah }}</td>
                          <td><a href="{{ route('order.edit',$item->id)  }}" name="submit" class="btn btn-success btn-sm">Edit</a></td>

                          {!! Form::open(['route'=>['order.destroy',$item->id], 'method'=>'Delete']) !!}

                          <td><button type="submit" name="submit" class="btn btn-danger btn-sm">Delete</button></td>
                          {!! Form::close() !!}
                          </tr>

                            <?php $no++; ?>
                          @endforeach


                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script>
    $('#users-table').DataTable();
    });
</script>
@endpush

